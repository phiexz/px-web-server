#include <ButtonConstants.au3>
#include <ComboConstants.au3>
#include <GUIConstantsEx.au3>
#include <StaticConstants.au3>
#include <WindowsConstants.au3>
#include <MsgBoxConstants.au3>
#include <File.au3>
#include <Misc.au3>

; Prevent standard Windows events
Opt("GUIEventOptions", 1)
; Remove standard tray menu items
Opt("TrayOnEventMode", 1)
Opt("TrayMenuMode", 3)

If _Singleton("PX Server Daemon", 1) = 0 Then
  MsgBox($MB_ICONERROR, "PX Server Daemon", "Application already running")
  Exit
EndIf

; Create tray menu
Global $cTray_Show = TrayCreateItem("Show")
TrayItemSetOnEvent($cTray_Show, "_Show_GUI")
TrayCreateItem("")
Global $cTray_Exit = TrayCreateItem("Exit")
TrayItemSetOnEvent($cTray_Exit, "_Exit")

#Region ### GUI Create ### Form=
$Form1 = GUICreate("PX Server Daemon", 650, 260)
GUISetIcon("E:\Multimedia\image\phiexz\icon\asdf [www.imagesplitter.net].ico", -1)
GUISetFont(12, 400, 0, "MS Sans Serif")
GUISetBkColor(0xFFFFFF)
$header = GUICtrlCreateLabel("PX SERVER DAEMON", 16, 16, 622, 41, $SS_CENTER)
GUICtrlSetFont(-1, 24, 800, 0, "MS Sans Serif")
$headerLine = GUICtrlCreateGraphic(13, 65, 628, 5, BitOR($GUI_SS_DEFAULT_GRAPHIC,$SS_BLACKRECT,$SS_SUNKEN))
$component = GUICtrlCreateLabel("Component", 32, 88, 104, 24, $SS_CENTER)
GUICtrlSetFont(-1, 12, 800, 0, "MS Sans Serif")
$componentVersion = GUICtrlCreateLabel("Version", 152, 88, 218, 22, $SS_CENTER)
GUICtrlSetFont(-1, 12, 800, 0, "MS Sans Serif")
$componentRunning = GUICtrlCreateLabel("Running?", 400, 88, 81, 24)
GUICtrlSetFont(-1, 12, 800, 0, "MS Sans Serif")
$componentAction = GUICtrlCreateLabel("Action", 504, 88, 119, 24, $SS_CENTER)
GUICtrlSetFont(-1, 12, 800, 0, "MS Sans Serif")
$nginx = GUICtrlCreateLabel("Nginx", 32, 136, 99, 20, $SS_CENTER)
GUICtrlSetFont(-1, 10, 800, 0, "MS Sans Serif")
$nginxVersion = GUICtrlCreateCombo("", 152, 136, 225, 25, BitOR($GUI_SS_DEFAULT_COMBO,$CBS_SIMPLE))
GUICtrlSetData(-1, "")
GUICtrlSetFont(-1, 10, 400, 0, "MS Sans Serif")
$nginxRunning = GUICtrlCreateLabel("NO", 400, 136, 82, 20, $SS_CENTER)
GUICtrlSetFont(-1, 10, 800, 0, "MS Sans Serif")
GUICtrlSetColor(-1, 0xFF0000)
$nginxActionStart = GUICtrlCreateButton("Start", 504, 128, 51, 33)
GUICtrlSetFont(-1, 10, 800, 0, "MS Sans Serif")
GUICtrlSetColor(-1, 0xFFFFFF)
GUICtrlSetBkColor(-1, 0x0000FF)
$nginxActionStop = GUICtrlCreateButton("Stop", 560, 128, 51, 33)
GUICtrlSetFont(-1, 10, 800, 0, "MS Sans Serif")
GUICtrlSetColor(-1, 0xFFFFFF)
GUICtrlSetBkColor(-1, 0xFF0000)
$php = GUICtrlCreateLabel("PHP", 32, 176, 99, 20, $SS_CENTER)
GUICtrlSetFont(-1, 10, 800, 0, "MS Sans Serif")
$phpVersion = GUICtrlCreateCombo("", 152, 176, 225, 25, BitOR($GUI_SS_DEFAULT_COMBO,$CBS_SIMPLE))
GUICtrlSetData(-1, "")
GUICtrlSetFont(-1, 10, 400, 0, "MS Sans Serif")
$phpRunning = GUICtrlCreateLabel("NO", 400, 176, 82, 20, $SS_CENTER)
GUICtrlSetFont(-1, 10, 800, 0, "MS Sans Serif")
GUICtrlSetColor(-1, 0xFF0000)
$phpActionStart = GUICtrlCreateButton("Start", 504, 168, 51, 33)
GUICtrlSetFont(-1, 10, 800, 0, "MS Sans Serif")
GUICtrlSetColor(-1, 0xFFFFFF)
GUICtrlSetBkColor(-1, 0x0000FF)
$phpActionStop = GUICtrlCreateButton("Stop", 560, 168, 51, 33)
GUICtrlSetFont(-1, 10, 800, 0, "MS Sans Serif")
GUICtrlSetColor(-1, 0xFFFFFF)
GUICtrlSetBkColor(-1, 0xFF0000)
$db = GUICtrlCreateLabel("Database", 32, 216, 99, 20, $SS_CENTER)
GUICtrlSetFont(-1, 10, 800, 0, "MS Sans Serif")
$dbVersion = GUICtrlCreateCombo("", 152, 216, 225, 25, BitOR($GUI_SS_DEFAULT_COMBO,$CBS_SIMPLE))
GUICtrlSetData(-1, "0.0.0.0")
GUICtrlSetFont(-1, 10, 400, 0, "MS Sans Serif")
$dbRunning = GUICtrlCreateLabel("NO", 400, 216, 82, 20, $SS_CENTER)
GUICtrlSetFont(-1, 10, 800, 0, "MS Sans Serif")
GUICtrlSetColor(-1, 0xFF0000)
$dbActionStart = GUICtrlCreateButton("Start", 504, 208, 51, 33)
GUICtrlSetFont(-1, 10, 800, 0, "MS Sans Serif")
GUICtrlSetColor(-1, 0xFFFFFF)
GUICtrlSetBkColor(-1, 0x0000FF)
$dbActionStop = GUICtrlCreateButton("Stop", 560, 208, 51, 33)
GUICtrlSetFont(-1, 10, 800, 0, "MS Sans Serif")
GUICtrlSetColor(-1, 0xFFFFFF)
GUICtrlSetBkColor(-1, 0xFF0000)
GUISetState(@SW_SHOW)
#EndRegion ### END GUI Create ###
 
;;; change gui before launch ;;;
;cari list version dari nginx, php & db
GUICtrlSetData($nginxVersion,getVersionList("nginx"))
GUICtrlSetData($phpVersion,getVersionList("php"))
GUICtrlSetData($dbVersion,getVersionList("db"))

;;cek if component run
checkComponentRun()

;refresh every 1000ms
AdlibRegister("checkComponentRun",1000)
While 1
  $nMsg = GUIGetMsg()
  Switch $nMsg
    Case $GUI_EVENT_CLOSE
      Exit
    Case $GUI_EVENT_MINIMIZE
      ; Hide GUI
      GUISetState(@SW_HIDE, $Form1)
      AdlibUnRegister("checkComponentRun")
    Case $GUI_EVENT_RESTORE
      AdlibRegister("checkComponentRun",1000)
    case $nginxActionStart
      startComponent("nginx")
    Case $nginxActionStop
      local $nginxProcLocation = _ProcessGetLocation(ProcessExists ("nginx.exe"))
      RunWait("bin\RunHiddenConsole.exe " & $nginxProcLocation & " -s stop -c ../../../conf/nginx/nginx.conf", StringTrimRight($nginxProcLocation,9))
    case $phpActionStart
      startComponent("php")
    case $phpActionStop
      RunWait("bin\RunHiddenConsole.exe taskkill /f /IM php-cgi.exe")
    case $dbActionStart
      startComponent("db")
    case $dbActionStop
      local $dbProcLocation = _ProcessGetLocation(ProcessExists ("mysqld.exe"))
      local $dbDir = StringTrimRight($dbProcLocation,10)
      RunWait("bin\RunHiddenConsole.exe " & $dbDir & "/mysqladmin -uroot shutdown")

  EndSwitch
WEnd

Func _Show_GUI()
  GUISetState(@SW_SHOW, $Form1)
  AdlibRegister("checkComponentRun",1000)
EndFunc

Func _Exit()
  Exit
EndFunc

Func getVersionList($sMsg)
  ;Local $folderList = _FileListToArray("bin/" & $sMsg, $sMsg & "-*", $FLTA_FOLDERS)
  Local $folderList = _FileListToArray("bin/" & $sMsg, "*" , $FLTA_FOLDERS)
    
  If @error = 1 Then
    MsgBox($MB_SYSTEMMODAL, "", $sMsg & " directory not found.")
    Exit
  EndIf
  ; Display the results returned by _FileListToArray.
  ;_ArrayDisplay($folderList, "$folderList")
  Local $a,$i
  For $i = 1 To $folderList[0]
    $a = $a & "|" & $folderList[$i]
  Next
  return $a
EndFunc

Func checkComponentRun()
  ;nginx
  If ProcessExists("nginx.exe") Then
    GUICtrlSetData($nginxRunning,"YES")
    GUICtrlSetColor($nginxRunning, 0x0000FF)
    GUICtrlSetState($nginxActionStart, $GUI_DISABLE)
    GUICtrlSetState($nginxActionStop, $GUI_ENABLE)
    GUICtrlSetState($nginxVersion, $GUI_DISABLE)
  Else
    GUICtrlSetData($nginxRunning,"NO")
    GUICtrlSetColor($nginxRunning, 0xFF0000)
    GUICtrlSetState($nginxActionStop, $GUI_DISABLE)
    GUICtrlSetState($nginxActionStart, $GUI_ENABLE)
    GUICtrlSetState($nginxVersion, $GUI_ENABLE)
  EndIf
  ;php
  If ProcessExists("php-cgi.exe") Then
    GUICtrlSetData($phpRunning,"YES")
    GUICtrlSetColor($phpRunning, 0x0000FF)
    GUICtrlSetState($phpActionStart, $GUI_DISABLE)
    GUICtrlSetState($phpActionStop, $GUI_ENABLE)
    GUICtrlSetState($phpVersion, $GUI_DISABLE)
  Else
    GUICtrlSetData($phpRunning,"NO")
    GUICtrlSetColor($phpRunning, 0xFF0000)
    GUICtrlSetState($phpActionStop, $GUI_DISABLE)
    GUICtrlSetState($phpActionStart, $GUI_ENABLE)
    GUICtrlSetState($phpVersion, $GUI_ENABLE)
  EndIf
  ;db
  If ProcessExists("mysqld.exe") Then
    GUICtrlSetData($dbRunning,"YES")
    GUICtrlSetColor($dbRunning, 0x0000FF)
    GUICtrlSetState($dbActionStart, $GUI_DISABLE)
    GUICtrlSetState($dbActionStop, $GUI_ENABLE)
    GUICtrlSetState($dbVersion, $GUI_DISABLE)
  Else
    GUICtrlSetData($dbRunning,"NO")
    GUICtrlSetColor($dbRunning, 0xFF0000)
    GUICtrlSetState($dbActionStop, $GUI_DISABLE)
    GUICtrlSetState($dbActionStart, $GUI_ENABLE)
    GUICtrlSetState($dbVersion, $GUI_ENABLE)
  EndIf
EndFunc

Func _ProcessGetLocation($iPID)
  Local $aProc = DllCall('kernel32.dll', 'hwnd', 'OpenProcess', 'int', BitOR(0x0400, 0x0010), 'int', 0, 'int', $iPID)
  If $aProc[0] = 0 Then Return SetError(1, 0, '')
  Local $vStruct = DllStructCreate('int[1024]')
  DllCall('psapi.dll', 'int', 'EnumProcessModules', 'hwnd', $aProc[0], 'ptr', DllStructGetPtr($vStruct), 'int', DllStructGetSize($vStruct), 'int_ptr', 0)
  Local $aReturn = DllCall('psapi.dll', 'int', 'GetModuleFileNameEx', 'hwnd', $aProc[0], 'int', DllStructGetData($vStruct, 1), 'str', '', 'int', 2048)
  If StringLen($aReturn[3]) = 0 Then Return SetError(2, 0, '')
  Return $aReturn[3]
EndFunc

Func startComponent($sMsg)
  local $ver = GUICtrlRead(Eval($sMsg & "Version"))
  If StringLen($ver) = 0 Then
    MsgBox(0,"Error","Please Select " &$sMsg& " version")
  Else
    If $sMsg = "nginx" Then
      ;run nginx
      ;Run("..\..\bin\RunHiddenConsole.exe ../nginx/" & $ver & "\nginx.exe", "nginx/" & $ver)
      ;Run("bin\RunHiddenConsole.exe ../" & $ver & "\nginx.exe", $ver)
      $PID=Run("bin\RunHiddenConsole.exe nginx.exe -c ../../../conf/nginx/nginx.conf", "bin/nginx/" & $ver)
    ElseIf $sMsg = "php" Then
      ;run php
      ;Run("bin\RunHiddenConsole.exe ../" &$ver & "\php-cgi.exe -b 127.0.0.1:9000 -c ..\" & $ver & "\php.ini", $ver)
      ;Run("bin\RunHiddenConsole.exe ..\php\" &$ver & "\php-cgi.exe -b 127.0.0.1:9000 -c ..\..\conf\php.ini", $ver)
      $PID=Run("bin\RunHiddenConsole.exe php-cgi.exe -b 127.0.0.1:9000 -c ../../../conf/php.ini", "bin/php/" & $ver)
    ElseIf $sMsg = "db" Then
      ;run db
      $PID=Run("bin\RunHiddenConsole.exe mysqld.exe --defaults-file=../../../../conf/" & $ver & ".ini --log-error=../../../../logs/db_error.log --pid-file=../../../../logs/db.pid --standalone", "bin/db/" & $ver & "/bin")
    EndIf
    
    If $PID = 0 Then
      MsgBox(0,"Run","Path error :(\nCant run " & $sMsg)
    EndIf
  EndIf
EndFunc
